from typing import List
from math import sqrt, ceil
from sys import exit, stdout
from time import time
from typing import Callable

def timer(func: Callable) -> Callable:
  """
    times a function
    return: Callable func
  """
  def inner(*args, **kwargs):
    starttime = time()
    result = func(*args, **kwargs)
    endtime = time()
    print(f"{func.__name__}: {round(endtime - starttime, 5)} second")
    
    return result
  
  return inner

class Seive(object):
  def __init__(self, size: int, bruteforce: bool = False) -> None:
    """
      input: int the size of the seive. this dictates how high of a number to calculate
        primes up to
    """
    self.size = size
    self.data = self.create_seive() if not bruteforce else self.create_seive_brute()

  @timer
  def create_seive_brute(self) -> List[bool]:
    """
      Brute force solution for creating a seive not recommended for use
      
      Complexity O(n**2)
    
      return: List representing seive of eratosthenes
    """
    seive = [i % 2 != 0 or i == 0 or i == 2 for i in range(self.size)]
    for index in range (5, len(seive)):
      seive[index] = is_prime(index)

    return seive

  @timer
  def create_seive(self) -> List[bool]:
    """
      Fast solution for creating a seive
      we create a list of Booleans then mark the multiples of each number False
      as this means they are not prime
      
      Complexity O(n)

      input: int the size of the seive. this dictates how high of a number to calculate
        primes up to
      return: List representing seive of eratosthenes
    """
    seive = [i % 2 != 0 or i == 0 or i == 2 for i in range(self.size)]
    for i in range(3, ceil(sqrt(len(seive))) + 1, 2):
      for j in range(i * i, len(seive), i):
        seive[j] = False

    return seive

  @timer
  def seive_passes(self) -> bool:
    """
      A method for validating that a seive is correct.
      This method is just used for proving the algorithm is correct. 
      This is basically recreating the brute force solution, and as such is not
      recommended to be run on large numbers. it will take a long time.
      
      Complexity O(n**2)

      return: bool if seive passed or not
    """
    for index, value in enumerate(self.data):
      self.progress(round((index / self.size) * 100, 2))
      
      if value != self.is_prime(index):
        return False
    
    self.progress(100.00, True)

    return True

  def progress(self, percent: float, done: bool = False) -> None:
    """
      print progress line
    """
    stdout.write('\x1b[2K')
    stdout.write(f'\r{percent}%')
    if done:
      stdout.write('\n') 

  def is_prime(self, num: int) -> bool:
    """
      Find if a number is prime

      Complexity O(n**2)

      input: int number to check is prime
      return: bool is number prime
    """
    for i in range(2, num):
      if num % i == 0:
        return False

    return True

size = 100000
seive = Seive(size // 100)
seive = Seive(size // 10)
seive = Seive(size)