from collections import Counter

class Solution:
  def longestPalindrome(self, s: str) -> int:
    counter = Counter(s)
    has_odd = False
    length = 0
    for key in counter:
      if counter[key] % 2 != 0:
        has_odd = True
      length += counter[key] // 2 * 2

    return length if has_odd == False else length + 1

solution = Solution()
length = solution.longestPalindrome('aabbccddeef')
print(length)