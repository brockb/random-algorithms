class Solution:
  def longestPalindrome(self, s: str) -> str:
    self.longest = ''
    
    if s == '':
      return ""
    
    if len(s) == 1:
      return s
    
    for index in range(len(s) - 1):
      if s[index] == s[index + 1]:
        self.expand(s, index, index + 2) # slicing is non-inclusive
      if s[index - 1] == s[index + 1]:
        self.expand(s, index - 1, index + 2) # slicing is non-inclusive

    return self.longest if self.longest != '' else s[0]

  def expand(self, s: str, start: int, end: int) -> int:
    while self.is_palindrome(s[start -1 :end + 1]) and len(s[start -1 :end + 1]) > len(s[start:end]):
      start -= 1
      end += 1

    self.set_longest(s[start:end])

  def set_longest(self, s: str) -> None:
    if len(s) > len(self.longest):
      self.longest = s

  def is_palindrome(self, s: str) -> bool:
    return s == s[::-1]