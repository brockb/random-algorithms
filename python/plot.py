points = [[1, 2], [3, -1], [2, 1], [2, 3]]

def find_closest(points, count, vertex):
  distances = []
  closest = []
  for point in points:
    distances.append(find_distance(point, vertex))

  while count < len(points):
    index = min_index(distances)
    del points[index]

  return points

def min_index(distances):
  min_value = -1
  for index in range(len(distances)):
    if min_value == -1 or distances[index] < min_value:
      min_value = distances[index]

  return min_value

def find_distance(target, vertex):
  target_x, target_y = target
  vertex_x, vertex_y = vertex

  height = target_y - vertex_y
  width = target_x - vertex_x

  return (height ** 2) + (width ** 2)

closest = find_closest(points, 4, [2, 2])
print(closest)