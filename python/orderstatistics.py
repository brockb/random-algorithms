class RankSort(object):
  def __init__(self):
    self.list = [1, 6, 3, 9, 8, 5]

  def smallest(self, index):
    newlist = []
    for number in self.list:
      if self.rank(number) == index - 1:
        return number

  def rank(self, target):
    rank = 0
    for number in self.list:
      if target > number:
        rank += 1

    return rank

class SearchTree(object):
  def __init__(self, numbers):
    self.numbers = numbers

  def smallest(self, target_index):
    start, end = 0, len(self.numbers)
    found_index = -1
    while target_index != found_index + start + 1:
      new_list, found_index = self.sort_first_element(self.numbers[start:end])
      self.numbers[start:end] = new_list
      print(self.numbers[start:end], start, end, found_index + start)
      if target_index < found_index + start + 1:
        end = found_index + start
      elif target_index > found_index + start + 1:
        start += found_index + 1

    return self.numbers[found_index + start]

  def sort_first_element(self, number_list):
    new_list = number_list[:]
    target = number_list[0]
    smallest = 0
    biggest = len(number_list) - 1
    for number in number_list:
      if number > target:
        new_list[biggest] = number
        biggest -= 1
      elif number < target:
        new_list[smallest] = number
        smallest += 1

    new_list[biggest] = target

    return [
      new_list,
      biggest
    ]

search_tree = SearchTree([10, 1, 8, 5, 11, 15, 4, 9, 2])
n = search_tree.smallest(9)
print(n)