class ListNode(object):
  def __init__(self, val: int) -> None:
    self.val = val
    self.next = None

class Solution(object):
  """
    solution to problem #83 on leetcode.com
    question: Given a sorted linked list, delete all duplicates such that each element appear only once.
  """
  def deleteDuplicates(self, head: ListNode) -> ListNode:
    """
      given the head of a sorted linked list will delete duplicate nodes
    """
    if head == None:
      return head
    
    current = head
    while current.next != None:
      if current.val == current.next.val:
        current.next = current.next.next
      else:
        current = current.next

    return head

help(Solution)